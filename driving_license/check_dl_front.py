import config as cfg
import json

def jaccard_similarity(query, document):
    intersection = set(query).intersection(set(document))
    union = set(query).union(set(document))
    return len(intersection)/len(union)



def get_details_dl_front(extracted_text):
    d = {'file_identified' : "dl_front.jpg", 'details': extracted_text}
    return d


#Check if it is Adhar card or not using both Adhar front & Adhar back
def check(extracted_text):
    if((jaccard_similarity(extracted_text[0], cfg.dl_identifier['DL_FRONT_IDENTIFIER'])) >= 0.5):
       return True
    else:
        return False
        

#l = ['INDIAN UNION DRIVING AUTHORITY', 'HARI ASOKAN', 'SHYAM BIHARI SWAMI']
#print(get_details_dl_front(l))
