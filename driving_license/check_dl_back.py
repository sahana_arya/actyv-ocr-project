import config as cfg

def jaccard_similarity(query, document):
    intersection = set(query).intersection(set(document))
    union = set(query).union(set(document))
    return len(intersection)/len(union)


def get_details_dl_back(extracted_text):
    d = {'file_identified' : 'dl_back.jpg', 'details': extracted_text}
    return d


#Check if it is Adhar card or not using both Adhar front & Adhar back
def check(extracted_text):
    if((jaccard_similarity(extracted_text[0], cfg.dl_identifier['DL_BACK_IDENTIFIER'])) >= 0.5):
       return True
    else:
        return False
        
#l = ['RTO']
#print(get_details_dl_back(l))


