
credentials = {
    'credential' : "GOOGLE_APPLICATION_CREDENTIALS",
    'path' : "credentials/googleCredentials/textrecog-credentials.json"
}
status = {
    'NOT_ADHAR' : "Not Authentic Adhar",
    'NOT_PAN' : "Not Authentic Pan",
    'NOT_DL' : "Not Authentic DL",
    'VERIFICATION_SUCCESS': "Verification was successful",
    'VERIFICATION_UNSUCCESS': "Verification was unsuccessful"
}

adhar_identifier = {
    'ADHAR_FRONT_IDENTIFIER' : "GOVERNMENT OF INDIA",
    'ADHAR_BACK_IDENTIFIER' : "UNIQUE IDENTIFICATION AUTHORITY OF INDIA"
}

pan_identifier = {
    'PAN_IDENTIFIER' : "INCOME TAX DEPARTMENT"
}

voterid_identifier = {
    'VOTERID_FRONT_IDENTIFIER' : "ELECTION COMMISSION OF INDIA",
    'VOTERID_BACK_IDENTIFIER' : "ELECTORAL REGISTRATION OFFICER"
}

upload_exceptions = {
    'NO_ADHAR_FRONT' : "No Adhar front file uploaded",
    'NO_ADHAR_BACK' : "No Adhar back file uploaded",
    'NO_PAN' : "No Pan card uploaded",

}

upload_success = {
    'ADHAR_FRONT' : "Adhar front upload success",
    'ADHAR_BACK' : "Adhar back upload success",
    'PAN' : "Pan upload success"
}

allowed_file = "Allowed file type is jpg"

file_names = [
    "adhar_front.jpg",
    "adhar_back.jpg",
    "pan.jpg",
    "dl_front.jpg",
    "dl_back.jpg"
]

uploaded_file_name = ['adhar_front_file', 'adhar_back_file',
                      'pan_file', 'dl_front_file', 'dl_back_file']

