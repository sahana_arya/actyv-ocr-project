import config as cfg
import json

#used to find the number of similar words between 2 sentences
def jaccard_similarity(query, document):
    intersection = set(query).intersection(set(document))
    union = set(query).union(set(document))
    return len(intersection)/len(union)


def get_details_pan(extracted_text):
   d = {'file_identified': "pan.jpg", 'details' : extracted_text}
   return d


#Check if it is Pan card or not
def check(pan_text):
    if(((jaccard_similarity(pan_text[0], cfg.pan_identifier['PAN_IDENTIFIER'])) >= 0.7)):
        return True
    else:
        return False

#l = ['INCOME TAX DEPARTMENT', 'OVT OF INDIA', 'HARI ASOKAN', 'ASOKAN', '25 12 1970', 'PERMANENT ACCOUNT NUMBER', 'AMAPH6226P', 'AHAMI', 'SIGNATURE']
#print(get_details_pan(l))