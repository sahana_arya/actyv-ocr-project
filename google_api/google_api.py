import os
import io
from google.cloud import vision
import requests




os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "credentials/googleCredentials/actyvGoogleCredentials.json"
MAX_RETRY_LIMIT = 5
retryNumber = 0



def detect_text(image_url):
    list_text = []
   
    content = requests.get(image_url).content
    client = vision.ImageAnnotatorClient()
    image = vision.types.Image(content=content)

    response = client.text_detection(image=image)
    texts = response.text_annotations

    for text in texts:
        list_text.append(format(text.description))
    global retryNumber
    if response.error.message:
        if retryNumber >= MAX_RETRY_LIMIT:
            raise Exception(
                  '{}\nFor more info on error messages, check: '
                  'https://cloud.google.com/apis/design/errors'.format(
                      response.error.message))
            
            return []
        retryNumber += 1
        return detect_text(image_url)
    
    
    return list_text



