import config as cfg
import re
import json


#converting list to string
def list_to_string(text, left_index, right_index):
    address =''
    while(left_index<=right_index):
        address = address + ' ' +  text[left_index]
        left_index = left_index+1
    return address


#getting the address, stopping condition is pincode
def get_address(text):
    address = ''
    index_pincode = 0
    for l in range(len(text)):
        if(re.search("^ADDRESS", text[l])):
            index_address = l
            while(index_pincode < index_address):
                address = address + text[index_address] + ' '
                index_address +=1
                if(re.search("\d{6}", text[index_address])):
                    index_pincode = l
                    pincode = re.findall('\d{6}', text[index_address])
                    add_before_pin = text[index_address].replace(pincode[0],'')
                    address  = address + ' ' + add_before_pin

                    return (address, pincode[0])
                
                else:
                    return ('','')


        return ('','')


#getting VoterID_back deatils
def get_details_voterid_back(extracted_text):
    address, pincode = get_address(extracted_text)
    d = {'fileIdentified' : "document_back.jpg", 'address':address, 'pincode': pincode}
    return d


#Check if it is VoterID or not using both VoterID front & VoterID back
def check(extracted_text):
    for i in range(len(extracted_text)):
        if(re.search(cfg.voterid_identifier['VOTERID_BACK_IDENTIFIER'], extracted_text[i])):
            return True
    return False



