import config as cfg
import re 
import datetime


def find_name(text):
    for i in range(len(text)):
        if(re.search("ELECTOR S NAME", text[i])):
            spl_word = 'ELECTOR S NAME'
            name = text[i].partition(spl_word)[2]
            if name == '':
                name = text[i+1]
            return name

        elif(re.search("NAME", text[i])):
                name = text[i+1]
                return name
    return ''

#finding date of birth/year of birth
def find_date(text):
    try:
        for l in range(len(text)):  
            if(re.search("AGE AS ON", text[l])):      
                dates = re.findall('\d+', text[l]) 
                today = datetime.datetime.now()
                age_after = today.year - int(dates[2])

                if(len(dates) >3):
                    age_before = dates[3]

                else:
                    while(l<len(text)):
                        if(re.findall("^\d{2}$", text[l])):
                            age_before = int(text[l])
                        l = l+1

                age = age_before + age_after
                return age

            elif(re.search("DATE OF BIRTH", text[l])):
                    dates = re.findall('\d+', text[l]) 
                    today = datetime.datetime.now()
                    age = today.year - int(dates[2])
                    return age

    except:
        return ''


#finding gender
def find_gender(text):
    for sentence in text:
        if(re.search("MALE", sentence)):
            return "MALE"
        elif(re.search("FEMALE", sentence)):
            return "FEMALE"
        else:
            pass


#get adhar number
def find_id_number(text):
    for i in range(len(text)):
        if(re.search("IDENTITY CARD", text[i])):
            spl_word = 'IDENTITY CARD'
            id = text[i].partition(spl_word)[2]
            id = id.replace(" ", "")
            if id == '':
                id = text[i+1]
            return id



#finding father name
def find_father_name(text):
    for l in range(len(text)):
        if(re.search("FATHER S NAME", text[l])):
            if(len(text[l]) > 13):
                father_name = (text[l])[14:]
                return father_name
            else:
                father_name = text[l-1]
                return father_name
    return ''



#getting details from adhar
def get_details_voterid_front(extracted_text):
    name = find_name(extracted_text)
    age = find_date(extracted_text)
    gender = find_gender(extracted_text)
    id_number = find_id_number(extracted_text)
    father_name = find_father_name(extracted_text)
    d = {'fileIdentified' : "document_front.jpg", 'name' : name, 'age' : age , 'father' : father_name, 'gender': gender, 'idNumber': id_number, 'idType': 'VOTER ID' }
    return d



#Check if it is Adhar card or not using both Adhar front & Adhar back
def check(extracted_text):
    for i in range(len(extracted_text)):
        if(re.search(cfg.voterid_identifier['VOTERID_FRONT_IDENTIFIER'], extracted_text[i])):
            return True
    return False
        


