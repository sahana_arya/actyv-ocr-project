import json
import codecs
import config as cfg


#used to find the number of similar words between 2 sentences
def jaccard_similarity(query, document):
    intersection = set(query).intersection(set(document))
    union = set(query).union(set(document))
    print(len(intersection)/len(union))
    return len(intersection)/len(union)


#Verifying Adhar name with Pan card name
def verify_name(adhar_name, pan, dl_front):
    for word in pan:
        if (jaccard_similarity(adhar_name, word) >= 0.7):
            for word1 in dl_front:
                if(jaccard_similarity(adhar_name, word1) >= 0.7):
                    return True
    return False



