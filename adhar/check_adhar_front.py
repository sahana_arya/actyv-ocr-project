import config as cfg
import re 
import datetime

def jaccard_similarity(query, document):
    intersection = set(query).intersection(set(document))
    union = set(query).union(set(document))
    return len(intersection)/len(union)

#finding date of birth/year of birth
def find_date(text):
    try:
        for l in range(len(text)):  
            if(re.search("\DOB", text[l])):    #SOME ADHAR'S HAVE DATE OF BIRTH   
                dates = re.findall('\d+', text[l])
                if(dates != []):
                    date = dates[0] + '/' + dates[1] + '/' + dates[2]
                    now = datetime.datetime.now()
                    age = now.year - int(dates[2])
                    return(date,age)
                else:
                    dates = re.findall('\d+', text[l+1])
                    date = dates[0] + '/' + dates[1] + '/' + dates[2]
                    now = datetime.datetime.now()
                    age = now.year - int(dates[2])
                    return(date, age)

            elif(re.search("YEAR OF BIRTH", text[l])):     #SOME ADHAR'S HAVE YEAR OF BIRTH
                dates = re.findall('\d+', text[l])          
                if(dates != []):
                    date = dates[0]
                    now = datetime.datetime.now()
                    age = now.year - int(date)
                    return (date, age)
                else:
                    dates = re.findall('\d+', text[l+1])
                    date = dates[0]
                    now = datetime.datetime.now()
                    age = now.year - int(date)
                    return (date, age)
            else:
                pass
    except:
        return ('','')


#finding gender
def find_gender(text):
    for sentence in text:
        if(re.search("MALE", sentence)):
            return "MALE"
        elif(re.search("FEMALE", sentence)):
            return "FEMALE"
        else:
            pass


#get adhar number
def find_adhar_number(text):
    for sentence in text:
        if(re.search("[0-9][0-9][0-9][0-9]\s[0-9][0-9][0-9][0-9]\s[0-9][0-9][0-9][0-9]", sentence)):
            sentence = sentence.replace(" ", "")
            return sentence
        elif(re.search("\d{4}\s\d{8}", sentence)):
            sentence = sentence.replace(" ", "")
            return sentence
        elif(re.search("\d{8}\s\d{4}", sentence)):
            sentence = sentence.replace(" ", "")
            return sentence


#finding father name
def find_father_name(text):
    for l in range(len(text)):
        if(re.search("FATHER", text[l])):
            if(len(text[l]) > 6):
                father_name = (text[l])[7:]
                return father_name
            else:
                father_name = text[l+1]
                return father_name
    return ''


#finding husband name
def find_husband_name(text):
    for l in range(len(text)):
        if(re.search("HUSBAND", text[l])):
            if(len(text[l]) > 7):
                father_name = (text[l])[8:]
                return father_name
            else:
                father_name = text[l+1]
                return father_name
    return ''


#getting details from adhar
def get_details_adhar_front(extracted_text):
    dob, age = find_date(extracted_text)
    gender = find_gender(extracted_text)
    adhar_number = find_adhar_number(extracted_text)
    father_name = find_father_name(extracted_text)
    husband_name = find_husband_name(extracted_text)
    d = {'fileIdentified' : "document_front.jpg", 'name' : extracted_text[1], 'dob': dob, 'age' : age , 'father' : father_name, 'husband': husband_name,'gender': gender, 'idNumber': adhar_number, }
    return d



#Check if it is Adhar card or not using both Adhar front & Adhar back
def check(extracted_text):
    if((jaccard_similarity(extracted_text[0], cfg.adhar_identifier['ADHAR_FRONT_IDENTIFIER'])) >= 0.7):
       return True
    else:
        return False
        


#l = ['GOVERNMENT OF INDIA', 'MAHAN CHINNASAMY', 'DOB 30 09 1997', 'MALE', '67801176 7754']
#̀̀print(get_details_adhar_front(l))