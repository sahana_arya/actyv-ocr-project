import config as cfg
import re
import json

def jaccard_similarity(query, document):
    intersection = set(query).intersection(set(document))
    union = set(query).union(set(document))
    return len(intersection)/len(union)


#converting list to string
def list_to_string(text, left_index, right_index):
    address =''
    while(left_index<=right_index):
        address = address + ' ' +  text[left_index]
        left_index = left_index+1
    return address



#getting the address, stopping condition is pincode
def get_address(text):
    address = ''
    for l in range(len(text)):
        if(re.search("^ADDRESS", text[l])):
            l = l + 1
            last_index = len(text)-1
            while(last_index>=l):
                if(re.search("\d{6}", text[last_index])):
                    pincode = re.findall('\d{6}', text[last_index])
                    address = list_to_string(text, l, last_index)
                    return (address, pincode[0])
                    
                last_index = last_index-1
        
        if(re.search("^TO", text[l])):
            print("here")
            while(l<len(text)):
                if(re.search("D O",text[l]) or re.search("S O",text[l]) or re.search("W O",text[l])):
                    last_index = len(text)-1
                    while(last_index>=l):
                        if(re.search("\d{6}", text[last_index])):
                            pincode = re.findall('\d{6}', text[last_index])
                            address = list_to_string(text, l, last_index)
                            return (address, pincode[0])
                            
                        last_index = last_index-1
                l = l + 1

    return ('','')

#getting adhar_back deatils
def get_details_adhar_back(extracted_text):
    address, pincode = get_address(extracted_text)
    d = {'fileIdentified' : "document_back.jpg", 'address':address, 'pincode': pincode}
    return d


#Check if it is Adhar card or not using both Adhar front & Adhar back
def check(extracted_text):
    if((jaccard_similarity(extracted_text[0], cfg.adhar_identifier['ADHAR_BACK_IDENTIFIER'])) >= 0.6):
       return True
    else:
        return False


# l =['UNIQUE IDENTIFICATION AUTHORITY OF INDIA', '855 ENROLMENT NO 2052 50581 01292', 'TO', 'SANGANI SHIVA PRASADH', 'S O SANGANI SANJU', '6 64 1', 'HANMANDLA GALLY', 'BODHAN MANDAL', 'SALOORA', 'SALOORA', 'NIZAMABAD TELANGANA 503185', '8185076997', 'DOWNLOAD DATE 05 03 2020', 'SS']
# print(get_details_adhar_back(l))
