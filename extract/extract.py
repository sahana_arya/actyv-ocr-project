#import test
import re
from google_api import google_api


#Preprocessing the text to get only English words & digits
def preprocess(text):
    list1 = []
    list2 = []
    list1 = text.split("\n")
    for word in list1:
        res1 = " ".join(re.findall("[a-zA-Z0-9]+", word))
        res1 = str(res1)
        if len(res1)>0:
            list2.append(res1.upper())
    return list2


#Extracting text using google Api
def extract_text(image_input):
    text = google_api.detect_text(image_input)
    if(len(text) >0):
        preprocessed_text = preprocess(text[0])
        return preprocessed_text
    return None

