import unittest
import main
import payload as py
from extract import extract
from adhar import check_adhar_front, check_adhar_back
from pan import check_pan
from driving_license import check_dl_back, check_dl_front


class TestSum(unittest.TestCase):

    def test_adhar_front(self):
        self.assertEqual(main.is_adhar_front(py.adhar_front_extracted_text), True)

    def test_adhar_back(self):
        self.assertEqual(main.is_adhar_back(py.adhar_back_extracted_text),True)

    def test_pan(self):
        self.assertEqual(main.is_pan(py.pan_extracted_text), True)

    def test_dl_front(self):
        self.assertEqual(main.is_dl_front(py.dl_front_extracted_text), True)

    def test_dl_back(self):
        self.assertEqual(main.is_dl_back(py.dl_back_extracted_text), True)


    def test_get_adhar_front(self):
        text = check_adhar_front.get_details_adhar_front(py.adhar_front_extracted_text)
        self.assertEqual(text , py.adhar_front_formatted)
    

    def test_get_adhar_back(self):
        text = check_adhar_back.get_details_adhar_back(py.adhar_back_extracted_text)
        self.assertEqual(text , py.adhar_back_formatted)


    def test_get_pan(self):
        text = check_pan.get_details_pan(py.pan_extracted_text)
        self.assertEqual(text, py.pan_formatted)


    def test_get_dl_front(self):
        text = check_dl_front.get_details_dl_front(py.dl_front_extracted_text)
        self.assertEqual(text, py.dl_front_formatted)
        
    
    def test_get_dl_back(self):
        text = check_dl_back.get_details_dl_back(py.dl_back_extracted_text)
        self.assertEqual(text, py.dl_back_formatted)



if __name__ == '__main__':
    unittest.main()