from extract import extract
from adhar import check_adhar_front, check_adhar_back
from pan import check_pan
from voterid import check_voterid_front, check_voterid_back
from verification import verify_name



def is_adhar_front(extracted_text):
    return check_adhar_front.check(extracted_text)


def is_adhar_back(extracted_text):
    return check_adhar_back.check(extracted_text)


def is_voterid_front(extracted_text):
    return check_voterid_front.check(extracted_text)


def is_voterid_back(extracted_text):
    return check_voterid_back.check(extracted_text)



ERROR_MESSAGE = ({"message" : 'Could not identify the image, please upload proper image'})
ERROR_MESSAGE_GOOGLE_API = ({"message" : 'Issue with the google api/could not find the image'})


#STARTS HERE
#getting the documents path
def steps(image_file):
    if image_file == '':
        return ERROR_MESSAGE_GOOGLE_API

    extracted_text = extract.extract_text(image_file)

    if not extracted_text :
        return ERROR_MESSAGE_GOOGLE_API

    if is_adhar_front(extracted_text):
        adhar_front_text = check_adhar_front.get_details_adhar_front(extracted_text)
        return adhar_front_text

    elif is_adhar_back(extracted_text):
        adhar_back_text = check_adhar_back.get_details_adhar_back(extracted_text)
        return adhar_back_text

    elif is_voterid_front(extracted_text):
        voterid_front_text = check_voterid_front.get_details_voterid_front(extracted_text)
        return voterid_front_text

    elif is_voterid_back(extracted_text):
        voterid_back_text = check_voterid_back.get_details_voterid_back(extracted_text)
        return voterid_back_text
    
    return ERROR_MESSAGE


