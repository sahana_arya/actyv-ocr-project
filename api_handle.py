import os
from flask import   Flask, flash, request, redirect, render_template, send_file
import config as cfg
import main
import json



app = Flask(__name__, template_folder='./template')


@app.route('/')
def home():
    return render_template('home.html')

    
#IDENTIFYING THE UPLOADED DOCUMENT
@app.route('/identify_document', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        file_url = json.loads(request.data)["file"]
        extracted_text = main.steps(file_url)
        return extracted_text
    else:
        return redirect(request.url)
    return redirect('/')


if __name__ == "__main__":
    app.run()